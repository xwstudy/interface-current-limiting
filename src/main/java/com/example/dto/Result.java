package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
/**
 *<p> </p>
 *返回结果类
 *@author panwu
 *@descripion
 *@date 2023/8/14 8:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    //是否成功
    private Boolean success;
    //返回错误信息类型
    private String erroMsg;
    private String code;
    //返回数据
    private Object data;
    //总数量
    private Long total;

    public static Result ok() {
        return new Result(true,null, null, null, null);
    }
    //成功只返回数据
    public static Result ok(Object data) {
        return new Result(true, null, null,data, null);
    }
    //成功返回数据和总数
    public static Result ok(List<?> data, Long total) {
        return new Result(true, null,null, data, total);
    }

    //返回异常
    public static Result fail(String code,String errorMsg) {
        return new Result(false, errorMsg, code,null, null);
    }


}
