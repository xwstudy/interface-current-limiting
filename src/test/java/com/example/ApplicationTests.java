package com.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

@SpringBootTest
class ApplicationTests {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void contextLoads() throws InterruptedException {

        ZSetOperations<String, String> zSetOperations = stringRedisTemplate.opsForZSet();
        for (int i = 0; i < 8; i++) {
            Thread.sleep(1);
            zSetOperations.add("test",String.valueOf(i),System.currentTimeMillis());
        }

    }
    @Test
    void contextLoads1() {
        long curTime = System.currentTimeMillis();
        Long a = curTime - 55 * 1000;
        System.out.println(a);
        ZSetOperations<String, String> zSetOperations = stringRedisTemplate.opsForZSet();
        Long test = zSetOperations.removeRangeByScore("test", 0, a);
        System.out.println(test);
    }

}
